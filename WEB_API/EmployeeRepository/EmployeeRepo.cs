﻿using EmployeeTechnology_DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using WEB_API.IEmployeeRepository;

namespace WEB_API.EmployeeRepository
{
    public class EmployeeRepo : IEmployeeRepo
    {
        private readonly ApplicationDBContext _dbContext;
        public EmployeeRepo(ApplicationDBContext dbContext)
        {
            _dbContext = dbContext;
        }
        public Employee CreateEmployee(Employee employee)
        {
            _dbContext.Employees.Add(employee);
            _dbContext.SaveChanges();
            return employee;
        }
        public IEnumerable<Employee> GetEmployees()
        {
            return _dbContext.Employees.ToList();
        }
         public Employee GetById(int id)
         {
            return _dbContext.Employees.SingleOrDefault(x => x.Id ==id);
         }

        public void UpdateEmployee(Employee employee)
        {
            _dbContext.Entry(employee).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }

        public void DeleteEmployee(int id)
        {
            
            var employee = _dbContext.Employees.Find(id);
            if (employee != null)
            {
                _dbContext.Entry(employee).State = EntityState.Deleted;
                _dbContext.SaveChanges();
            }
        }

        public Employee GetEmpById(int id)
        {
            var employee = _dbContext.Employees.Where(x => x.Id == id).Select(x => new Employee()
            {
                Emp_Name = x.Emp_Name,
                Role = x.Role,
                DOJ = x.DOJ,
                Salary = x.Salary

            }).FirstOrDefault();
            return employee;
        }

    }
}
