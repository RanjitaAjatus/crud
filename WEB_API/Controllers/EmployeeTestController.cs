﻿using EmployeeTechnology_DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WEB_API.IEmployeeRepository;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WEB_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeTestController : ControllerBase
    {
        private readonly IEmployeeRepo _employeeRepo;
        public EmployeeTestController(IEmployeeRepo employeeRepo)
        {
            _employeeRepo = employeeRepo;
        }
        // GET: api/<EmployeeTestController>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_employeeRepo.GetEmployees());
        }

        // GET api/<EmployeeTestController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var Emp = _employeeRepo.GetById(id);
            if (Emp != null)
            {
                return Ok(Emp);
            }
            return Ok($"{id} is not found !!!");
        }

        // POST api/<EmployeeTestController>
        [HttpPost]
        public IActionResult Post([FromBody] Employee employee)
        {

            var emp = _employeeRepo.CreateEmployee(employee);
            if (emp.Id == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Something Went Wrong!!! Try After sometime...");
            }
            return Ok("New Employee Added");
        }

        // PUT api/<EmployeeTestController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Employee employee)
        {
            _employeeRepo.UpdateEmployee(employee);
            if (id != employee.Id)
            {
                return NoContent();
            }

            return Ok("Updated Successfully!!!!!!");
        }

        // DELETE api/<EmployeeTestController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {

            _employeeRepo.DeleteEmployee(id);

            return Ok("Deleted Successfully!!!");
        }
    }
}
