﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB_API.IEmployeeRepository;
using WEB_API.Models;

namespace WEB_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeRepo _employeeRepo;
        public EmployeeController(IEmployeeRepo employeeRepo)
        {
            _employeeRepo = employeeRepo;
        }

        [HttpGet]
        //[Route("GetEmployee")]
        public IActionResult GetEmployee()
        {
            return Ok(_employeeRepo.GetEmployees());
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var Emp = _employeeRepo.GetById(id);
            if(Emp!= null)
            {
                return Ok(Emp);
            }
            return Ok($"{id} is not found !!!");
        }

        [HttpPost]
        public IActionResult AddNewEmp([FromBody] EmployeeDAL employee)
        {
            var emp = _employeeRepo.CreateEmployee(employee);
            if (emp.Id == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Something Went Wrong!!! Try After sometime...");
            }
            return Ok("New Employee Added");
        }

        [HttpPut("{id}")]
        [Route("UpdateEmployee")]
        public IActionResult UpdateEmployee(EmployeeDAL employee,int id)
        {
            _employeeRepo.UpdateEmployee(employee);
            if (id!=employee.Id)
            {
                return NoContent();
            }
            
            return Ok("Updated Successfully!!!!!!");
        }

        [HttpDelete]
        public IActionResult DeleteEmployee(int id)
        {
            _employeeRepo.DeleteEmployee(id);

            return Ok("Deleted Successfully!!!");
        }

    }
}
