﻿using EmployeeTechnology_DAL.Models;
using System.Collections.Generic;

namespace WEB_API.IEmployeeRepository
{
    public interface IEmployeeRepo
    {
        IEnumerable<Employee> GetEmployees();
        public Employee GetById(int id);
        public Employee CreateEmployee(Employee employee);
        public void UpdateEmployee(Employee employee);
        public void DeleteEmployee(int id);
    }
}
