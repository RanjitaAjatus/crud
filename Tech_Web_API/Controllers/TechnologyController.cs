﻿using EmployeeTechnology_DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tech_Web_API.ITechnologyRepository;

namespace Tech_Web_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TechnologyController : ControllerBase
    {
        private readonly ITechnologyRepo _technologyRepo;
        public TechnologyController(ITechnologyRepo technologyRepo)
        {
            _technologyRepo = technologyRepo;
        }

        [HttpGet]
       public IActionResult Get()
       {
           return Ok(_technologyRepo.GetAllTechnology());
       }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var Emp = _technologyRepo.GetById(id);
            if(Emp!= null)
            {
                return Ok(Emp);
            }
            return Ok($"{id} id not found!!!");
        }

        [HttpPost]
        
        public IActionResult Post([FromBody] Technology technology)
        {
            var tech = _technologyRepo.CreateTechnology(technology);
            if(tech.Id==0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Something Went Wrong!!! Try After sometime...");
            }
            return Ok("New Technology Added");
        }


        [HttpPut("{id}")]
        
        public IActionResult Put(Technology technology, int id)
        {
            _technologyRepo.UpdateTechnology(technology);
            if (id != technology.Id)
            {
                return NoContent();
            }

            return Ok("Updated Successfully!!!!!!");
        }

        [HttpDelete("{id}")]
        
        public IActionResult Delete(int id)
        {
            _technologyRepo.DeleteTechnology(id);

            return Ok($"{id} is Deleted!!!!!!!!!!!!!!!!!");
        }

    }
}
