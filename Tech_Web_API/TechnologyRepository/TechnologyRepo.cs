﻿using EmployeeTechnology_DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Tech_Web_API.ITechnologyRepository;

namespace Tech_Web_API.TechnologyRepository
{
    public class TechnologyRepo : ITechnologyRepo
    {
        private readonly ApplicationDBContext _techDb;
        public TechnologyRepo(ApplicationDBContext techDb)
        {
            _techDb = techDb;
        }
        public Technology CreateTechnology(Technology technology)
        {
            _techDb.Technologies.Add(technology);
            _techDb.SaveChanges();
            return technology;
        }

        public void DeleteTechnology(int id)
        {
            var technology = _techDb.Technologies.Find(id);
            if(technology!=null)
            {
                _techDb.Entry(technology).State = EntityState.Deleted;
                _techDb.SaveChanges();
            }
           
        }

        public IEnumerable<Technology> GetAllTechnology()
        {
            return _techDb.Technologies.ToList();
        }

        public Technology GetById(int id)
        {
            return _techDb.Technologies.SingleOrDefault(x =>x.Id ==id);
        }

        public void UpdateTechnology(Technology technology)
        {
            _techDb.Entry(technology).State = EntityState.Modified;
            _techDb.SaveChanges();
        }
    }
}
