﻿using EmployeeTechnology_DAL.Models;
using System.Collections.Generic;

namespace Tech_Web_API.ITechnologyRepository
{
    public interface ITechnologyRepo
    {
        IEnumerable<Technology> GetAllTechnology();
        public Technology GetById(int id);
        public Technology CreateTechnology(Technology employee);
        public void UpdateTechnology(Technology employee);
        public void DeleteTechnology(int id);
    }
}
