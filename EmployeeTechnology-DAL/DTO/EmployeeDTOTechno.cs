﻿using EmployeeTechnology_DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeTechnology_DAL.DTO
{
    public class EmployeeDTOTechno
    {
        public EmployeeDTOTechno()
        {
            Technologies = new List<TechnologyDTO>();
        }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeRole { get; set; }
        public DateTime EmployeeDOJ { get; set; }
        public float EmployeeSalary { get; set; }
        public ICollection<TechnologyDTO> Technologies { get; set; }
    }

    public class TechnologyDTO
    {
        public int TechnologyId { get; set; }
        public string Technology_Name { get; set; }
    }
}
