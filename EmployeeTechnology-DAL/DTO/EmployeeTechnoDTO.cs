﻿using EmployeeTechnology_DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeTechnology_DAL.DTO
{
   public class EmployeeTechnoDTO
    {
        public int EmployeeId { get; set; }
        public string Emp_Name { get; set; }
        public string Role { get; set; }
        public DateTime DOJ { get; set; }
        public float Salary { get; set; }
        public int TechnologyId { get; set; }
        public string Technology_Name { get; set; }

    }
}
