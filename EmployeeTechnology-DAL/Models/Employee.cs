﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EmployeeTechnology_DAL.Models
{
   public class Employee
    {

        [Key]
        public int Id { get; set; }
        public string Emp_Name { get; set; }
        public string Role { get; set; }
        public DateTime DOJ { get; set; }
        public float Salary { get; set; }
        public IList<EmployeeTechnology> EmployeeTechnologies { get; set; }
    }
}
