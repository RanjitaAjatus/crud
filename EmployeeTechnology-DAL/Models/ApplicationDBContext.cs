﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace EmployeeTechnology_DAL.Models
{
    public class ApplicationDBContext:DbContext
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {
        }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Technology> Technologies { get; set; }
        public DbSet<EmployeeTechnology> EmployeeTechnologies { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EmployeeTechnology>()
                .HasKey(bc => new { bc.EmployeeId, bc.TechnologyId });
            modelBuilder.Entity<EmployeeTechnology>()
                .HasOne(bc => bc.Employee)
                .WithMany(b => b.EmployeeTechnologies)
                .HasForeignKey(bc => bc.EmployeeId);
            modelBuilder.Entity<EmployeeTechnology>()
                .HasOne(bc => bc.Technology)
                .WithMany(c => c.EmployeeTechnologies)
                .HasForeignKey(bc => bc.TechnologyId);
        }
    }
}
