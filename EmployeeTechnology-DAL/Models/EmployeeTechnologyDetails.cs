﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EmployeeTechnology_DAL.Models
{
    public class EmployeeTechnologyDetails
    {
        [Key]
        public int Id { get; set; }
        public string EmployeeName { get; set; }
        public string Salary { get; set; }
   
    }
}
