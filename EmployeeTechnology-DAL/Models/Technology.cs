﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EmployeeTechnology_DAL.Models
{
    public class Technology
    {
        [Key]
        public int Id { get; set; }
        public string Technology_Name { get; set; }
        public IList<EmployeeTechnology> EmployeeTechnologies { get; set; }
    }
}
