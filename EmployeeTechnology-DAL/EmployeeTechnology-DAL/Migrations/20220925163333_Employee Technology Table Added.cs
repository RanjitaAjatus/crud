﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeeTechnology_DAL.EmployeeTechnologyDAL.Migrations
{
    public partial class EmployeeTechnologyTableAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Id",
                table: "EmployeeTechnologies");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "EmployeeTechnologies",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
