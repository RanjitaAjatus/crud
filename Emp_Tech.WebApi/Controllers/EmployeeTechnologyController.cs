﻿using Emp_Tech.WebApi.Repository.Irepo;
using EmployeeTechnology_DAL.DTO;
using EmployeeTechnology_DAL.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Tech_Web_API.ITechnologyRepository;
using WEB_API.IEmployeeRepository;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Emp_Tech.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeTechnologyController : ControllerBase
    {
        private readonly IEmployeeTechnology _EmployeeTechRepo;
        private readonly IEmployeeRepo _employeeRepo;
        private readonly ITechnologyRepo _technologyRepo;
        public EmployeeTechnologyController(IEmployeeTechnology EmployeeTechRepo, IEmployeeRepo employeeRepo, ITechnologyRepo technologyRepo)
        {
            _EmployeeTechRepo = EmployeeTechRepo;
            _employeeRepo = employeeRepo;
            _technologyRepo = technologyRepo;

        }


       // GET: api/<EmployeeTechnologyController>
        [HttpGet]
        public IActionResult Get()
        {
            var data =  _EmployeeTechRepo.GetAll();
            ICollection<EmployeeTechnoDTO> dataobject = new List<EmployeeTechnoDTO>();
            //var datajson = JsonSerializer.Serialize(data);
            foreach(var item in data)
            {
                dataobject.Add(new EmployeeTechnoDTO()
                {
                    EmployeeId=item.Employee.Id,
                    Emp_Name=item.Employee.Emp_Name,
                    Role=item.Employee.Role,
                    DOJ=item.Employee.DOJ,
                    Salary=item.Employee.Salary,
                    TechnologyId=item.Technology.Id,
                    Technology_Name=item.Technology.Technology_Name,
                   

                });
            }
            return Ok(dataobject);
        }



        //GET api/<EmployeeTechnologyController>/5
        [HttpGet("{empid}")]
        [Route("GetEmpTechById")]
        public IActionResult GetEmpTechById(int empid)
        {
            var data = _EmployeeTechRepo.GetByEmployeeId(empid);
            ICollection<TechnologyDTO> dataobject = new List<TechnologyDTO>();
            var objectdb = new EmployeeDTOTechno();
            var emp = data.Select(a => a.Employee).FirstOrDefault();
            EmployeeDTOTechno objectdto = new EmployeeDTOTechno()
            {
                EmployeeId = emp.Id,

                EmployeeName = emp.Emp_Name,
                EmployeeRole = emp.Role,
                EmployeeDOJ = emp.DOJ,
                EmployeeSalary = emp.Salary

            };
            foreach(var i in data)
            {
                dataobject.Add(new TechnologyDTO
                {
                    TechnologyId = i.Technology.Id,
                    Technology_Name = i.Technology.Technology_Name
                });
            }
            objectdto.Technologies = dataobject;
            return Ok(objectdto);
        }



        // POST api/<EmployeeTechnologyController>
        [HttpGet("{empid}/{techid}")]
        public IActionResult Get(int empid, int techid)
        {
            var emp = _employeeRepo.GetById(empid);
            var tech = _technologyRepo.GetById(techid);
            var data = new EmployeeTechnology()
            {
                Employee = emp,
                Technology = tech
            };
            _EmployeeTechRepo.SaveEmployeeTech(data);
            return Ok();
        }

        // PUT api/<EmployeeTechnologyController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EmployeeTechnologyController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
