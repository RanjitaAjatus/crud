﻿using EmployeeTechnology_DAL.DTO;
using EmployeeTechnology_DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Emp_Tech.WebApi.Repository.Irepo
{
    public interface IEmployeeTechnology
    {
        public void SaveEmployeeTech(EmployeeTechnology employeeTechnology);
        public IEnumerable<EmployeeTechnology> GetByEmployeeId(int empid);
        public EmployeeTechnology GetByTechnologyId(int techid);
        public IEnumerable<EmployeeTechnology> GetAll();
    }
}
