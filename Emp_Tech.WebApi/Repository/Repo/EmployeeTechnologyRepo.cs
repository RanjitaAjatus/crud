﻿using Emp_Tech.WebApi.Repository.Irepo;
using EmployeeTechnology_DAL.DTO;
using EmployeeTechnology_DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Emp_Tech.WebApi.Repository.Repo
{
    public class EmployeeTechnologyRepo : IEmployeeTechnology
    {
        private readonly ApplicationDBContext _dbContext;
        public EmployeeTechnologyRepo(ApplicationDBContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IEnumerable<EmployeeTechnology> GetAll()
        {
            return _dbContext.EmployeeTechnologies.Include(a=>a.Employee).Include(a=>a.Technology).ToList();
        }

        public IEnumerable<EmployeeTechnology> GetByEmployeeId(int empid)
        {
            var data = _dbContext.EmployeeTechnologies.Where(e=>e.EmployeeId==empid).Include(e=>e.Employee).Include(t=>t.Technology).ToList();

            return data;

        }

        public EmployeeTechnology GetByTechnologyId(int techid)
        {
            throw new NotImplementedException();
        }

        public void SaveEmployeeTech(EmployeeTechnology employeeTechnology)
        {

            _dbContext.EmployeeTechnologies.Add(employeeTechnology);
            _dbContext.SaveChanges();
            //return employeeTechnology;
        }
    }
}
